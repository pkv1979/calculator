import styles from "./ParametersStep.module.css";

import Steps from "../Steps";
import classNames from "classnames";

const ParametersStep = (props) => {
  const { info, updateInfo, steps, changeActiveStep, currentLang } = props;

  const clickNextBtn = () => {
    changeActiveStep(2);
  };

  const changeCity = (e) => {
    updateInfo("city", e.target.value);
  };

  const changeObjectStatus = (e) => {
    updateInfo("objectStatus", e.target.value);
  };

  const changeStreet = (e) => {
    updateInfo("street", e.target.value);
  };

  const changeCeilingHeight = (e) => {
    updateInfo("ceilingHeight", e.target.value);
  };

  return (
    <div
      id="parameters"
      className={classNames(styles.calculatorParameters, "calculatorFieldset")}
    >
      <div className="content">
        <h2 className="calculator-title">
          {currentLang === "ru-RU"
            ? "Рассчитайте стоимость"
            : "Розрахуйте вартість"}{" "}
          <br />
          <span className={styles.calculatorParametersTitleOrange}>
            {currentLang === "ru-RU" ? "ремонта под ключ" : "ремонту під ключ"}
          </span>
        </h2>
        <p className="calculator-subtitle">
          {currentLang === "ru-RU"
            ? "Стоимость с учетом работ и материалов"
            : "Вартість з урахуванням робіт і матеріалів"}
        </p>
        <div
          className={classNames("calculator-wrapper", styles.wrapperParameters)}
        >
          <div className={styles.formComponent}>
            <label className={styles.formLabel}>
              {currentLang === "ru-RU" ? "Город" : "Місто"}
            </label>
            <input
              type="text"
              className={styles.formInputText}
              value={info.city}
              placeholder={
                currentLang === "ru-RU" ? "Введите город" : "Введіть місто"
              }
              onChange={changeCity}
            />
          </div>

          <div className={styles.formComponent}>
            <label className={styles.formLabel}>
              {currentLang === "ru-RU" ? "Статус объекта" : "Статус об'єкта"}
            </label>
            <select
              className={styles.formSelect}
              value={info.objectStatus}
              onChange={changeObjectStatus}
            >
              <option
                value={currentLang === "ru-RU" ? "Новостройка" : "Новобудова"}
              >
                {currentLang === "ru-RU" ? "Новостройка" : "Новобудова"}
              </option>
              <option
                value={
                  currentLang === "ru-RU" ? "Вторичное жилье" : "Вторинне житло"
                }
              >
                {currentLang === "ru-RU" ? "Вторичное жилье" : "Вторинне житло"}
              </option>
              <option
                value={
                  currentLang === "ru-RU" ? "Частный дом" : "Приватний будинок"
                }
              >
                {currentLang === "ru-RU" ? "Частный дом" : "Приватний будинок"}
              </option>
            </select>
            <div className={styles.arrowDownRight}></div>
            <div className={styles.arrowDownLeft}></div>
          </div>

          <div className={styles.formComponent}>
            <label className={styles.formLabel}>
              {currentLang === "ru-RU" ? "Улица или ЖК" : "Вулиця або ЖК"}
            </label>
            <input
              type="text"
              className={styles.formInputText}
              value={info.street}
              placeholder={
                currentLang === "ru-RU"
                  ? "Введите улицу или ЖК"
                  : "Введіть вулицю або ЖК"
              }
              onChange={changeStreet}
            />
          </div>

          <div className={styles.formComponent}>
            <label className={styles.formLabel}>
              {currentLang === "ru-RU" ? "Высота потолка" : "Висота стелі"}
            </label>
            <select
              className={styles.formSelect}
              value={info.ceilingHeight}
              onChange={changeCeilingHeight}
            >
              <option value={currentLang === "ru-RU" ? "До 3 м" : "До 3 м"}>
                {currentLang === "ru-RU" ? "До 3 м" : "До 3 м"}
              </option>
              <option
                value={currentLang === "ru-RU" ? "Более 3 м" : "Понад 3 м"}
              >
                {currentLang === "ru-RU" ? "Более 3 м" : "Понад 3 м"}
              </option>
            </select>
            <div className={styles.arrowDownRight}></div>
            <div className={styles.arrowDownLeft}></div>
          </div>
        </div>
      </div>

      <div className="calculator-form-bottom">
        <div></div>

        <Steps steps={steps} activeStep="1" />

        <button className="calculator-new__btn btn-next" onClick={clickNextBtn}>
          <span>
            {currentLang === "ru-RU" ? "Выбрать комнаты" : "Вибрати кімнати"}
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="8"
              viewBox="0 0 24 8"
              fill="#fff"
            >
              <path d="M23.366 4.35355C23.5613 4.15829 23.5613 3.84171 23.366 3.64645L20.1841 0.464466C19.9888 0.269204 19.6722 0.269204 19.4769 0.464466C19.2817 0.659728 19.2817 0.976311 19.4769 1.17157L22.3054 4L19.4769 6.82843C19.2817 7.02369 19.2817 7.34027 19.4769 7.53553C19.6722 7.7308 19.9888 7.7308 20.1841 7.53553L23.366 4.35355ZM0 4.5H23.0125V3.5H0V4.5Z" />
            </svg>
          </span>
        </button>
      </div>
    </div>
  );
};

export default ParametersStep;
