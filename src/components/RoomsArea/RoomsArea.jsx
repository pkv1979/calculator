import classNames from "classnames";
import styles from "./RoomsArea.module.css";

const RoomsArea = (props) => {
  const { id, rooms, changeRoomArea, currentLang } = props;

  const changeArea = (e) => {
    changeRoomArea(id, +e.target.value);
  };

  const roomData = rooms.filter((room) => room.id === id);
  const area = roomData[0].area;

  return (
    <div className={classNames("roomsBlock", styles.roomsArea)}>
      <label className={styles.roomsAreaLabel}>
        {currentLang === "ru-RU" ? "Площадь" : "Площа"}
      </label>
      <input
        type="number"
        className={styles.roomsAreaInput}
        value={area}
        onChange={changeArea}
        onFocus={(e) => e.target.select()}
      />
      <span>
        м<sup>2</sup>
      </span>
    </div>
  );
};

export default RoomsArea;
