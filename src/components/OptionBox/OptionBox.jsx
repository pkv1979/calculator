import { useState } from "react";
import classNames from "classnames";

import styles from "./OptionBox.module.css";

const OptionBox = (props) => {
  const { optionRoom, imageStyle, changeOptionBoxTypeActive } = props;

  const [activeList, setActiveList] = useState(false);

  const toogleOptionListVisible = () => {
    setActiveList(!activeList);
  };

  return (
    <>
      <div className={styles.options} style={optionRoom.style}>
        <div className={styles.optionBox} onClick={toogleOptionListVisible}>
          <div className={styles.optionDot}></div>
          <div>{optionRoom.name}</div>
          <ul
            className={classNames(
              styles.optionsList,
              activeList ? styles.active : ""
            )}
            style={optionRoom.listStyle}
          >
            {optionRoom.types.map((item) => (
              <li
                key={item.id}
                className={classNames(
                  styles.optionListItem,
                  item.isSelected ? styles.active : ""
                )}
                onClick={() =>
                  changeOptionBoxTypeActive(optionRoom.id, item.id)
                }
              >
                {item.name}
              </li>
            ))}
          </ul>
        </div>
      </div>
      {optionRoom.types.map((item) => {
        if (item.image && item.isSelected) {
          return (
            <img
              key={item.id}
              src={item.image.src}
              className={styles.optionImage}
              style={imageStyle}
              alt={item.id}
              loading="lazy"
            />
          );
        }
      })}
    </>
  );
};

export default OptionBox;
