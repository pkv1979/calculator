import classNames from "classnames";

import RoomsComponent from "../RoomsComponent/RoomsComponent";
import Steps from "../Steps";

import styles from "./RoomsStep.module.css";

const RoomsStep = (props) => {
  const {
    apartmentsArea,
    rooms,
    steps,
    changeStepVisible,
    changeVisibleRoom,
    changeRoomArea,
    changeActiveStep,
    currentLang,
  } = props;

  const clickNextBtn = () => {
    changeActiveStep(3);
  };

  const clickPrevBtn = () => {
    changeActiveStep(1);
  };

  return (
    <div
      id="rooms"
      className={classNames(styles.calculatorRooms, "calculatorFieldset")}
    >
      <div className="content">
        <h2 className="calculator-title">
          {currentLang === "ru-RU" ? "Выбор комнат" : "Вибір кімнат"}
        </h2>
        <p className="calculator-subtitle">
          {currentLang === "ru-RU" ? "Общая площадь" : "Загальна площа"}{" "}
          {apartmentsArea} м<sup>2</sup>
        </p>

        <div className={classNames("calculator-wrapper", styles.roomsWrapper)}>
          <RoomsComponent
            room={currentLang === "ru-RU" ? "Студия" : "Студія"}
            room_id="studio"
            maxRoom="1"
            rooms={rooms.filter((room) => room.id.startsWith("studio"))}
            enableAddRoom={rooms.filter((room) =>
              room.id.startsWith("kitchen")
            )}
            changeStepVisible={changeStepVisible}
            changeVisibleRoom={changeVisibleRoom}
            changeRoomArea={changeRoomArea}
            currentLang={currentLang}
          />
          <RoomsComponent
            room={currentLang === "ru-RU" ? "Кухня" : "Кухня"}
            room_id="kitchen"
            maxRoom="1"
            rooms={rooms.filter((room) => room.id.startsWith("kitchen"))}
            enableAddRoom={rooms.filter((room) => room.id.startsWith("studio"))}
            changeStepVisible={changeStepVisible}
            changeVisibleRoom={changeVisibleRoom}
            changeRoomArea={changeRoomArea}
            currentLang={currentLang}
          />
          <RoomsComponent
            room={currentLang === "ru-RU" ? "Комната" : "Кімната"}
            room_id="room"
            maxRoom="3"
            rooms={rooms.filter((room) => room.id.startsWith("room"))}
            changeStepVisible={changeStepVisible}
            changeVisibleRoom={changeVisibleRoom}
            changeRoomArea={changeRoomArea}
            currentLang={currentLang}
          />
          <RoomsComponent
            room={currentLang === "ru-RU" ? "Санузел" : "Санвузол"}
            room_id="bathroom"
            maxRoom="2"
            rooms={rooms.filter((room) => room.id.startsWith("bathroom"))}
            changeStepVisible={changeStepVisible}
            changeVisibleRoom={changeVisibleRoom}
            changeRoomArea={changeRoomArea}
            currentLang={currentLang}
          />

          <div className={styles.buttonWrapper}></div>
        </div>
      </div>

      <div className="calculator-form-bottom">
        <button
          className={classNames("calculator-new__btn btn-prev", styles.btnPrev)}
          onClick={clickPrevBtn}
        >
          <span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="25"
              height="8"
              viewBox="0 0 25 8"
              fill="#000"
            >
              <path d="M0.64666 3.64645C0.451399 3.84171 0.451399 4.15829 0.64666 4.35355L3.82864 7.53553C4.0239 7.7308 4.34048 7.7308 4.53575 7.53553C4.73101 7.34027 4.73101 7.02369 4.53575 6.82843L1.70732 4L4.53575 1.17157C4.73101 0.976311 4.73101 0.659728 4.53575 0.464466C4.34048 0.269204 4.0239 0.269204 3.82864 0.464466L0.64666 3.64645ZM24.0127 3.5L1.00021 3.5V4.5L24.0127 4.5V3.5Z" />
            </svg>
            {currentLang === "ru-RU" ? "Назад" : "Назад"}
          </span>
        </button>

        <Steps steps={steps} activeStep="2" />

        <button
          className={classNames("calculator-new__btn btn-next", styles.btn)}
          onClick={clickNextBtn}
        >
          <span>
            {currentLang === "ru-RU" ? "Состояние помещений" : "Стан приміщень"}
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="8"
              viewBox="0 0 24 8"
              fill="#fff"
            >
              <path d="M23.366 4.35355C23.5613 4.15829 23.5613 3.84171 23.366 3.64645L20.1841 0.464466C19.9888 0.269204 19.6722 0.269204 19.4769 0.464466C19.2817 0.659728 19.2817 0.976311 19.4769 1.17157L22.3054 4L19.4769 6.82843C19.2817 7.02369 19.2817 7.34027 19.4769 7.53553C19.6722 7.7308 19.9888 7.7308 20.1841 7.53553L23.366 4.35355ZM0 4.5H23.0125V3.5H0V4.5Z" />
            </svg>
          </span>
        </button>
      </div>
    </div>
  );
};

export default RoomsStep;
