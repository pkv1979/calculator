import classNames from "classnames";
import { useEffect } from "react";

import styles from "./KitchenStep.module.css";
import OptionBox from "../OptionBox/OptionBox";
import Steps from "../Steps";

const KitchenStep = (props) => {
  const {
    steps,
    room,
    cost,
    changeActiveStep,
    changeRoomOptions,
    updateRoomCost,
    currentLang,
  } = props;

  const clickNextBtn = () => {
    changeActiveStep(nextStep.id);
  };

  const clickPrevBtn = () => {
    changeActiveStep(3);
  };

  let nextStep = {};
  if (steps[4].visible) {
    nextStep = steps[4];
  } else if (steps[7].visible) {
    nextStep = steps[7];
  } else if (steps[9].visible) {
    nextStep = steps[9];
  } else {
    nextStep = steps[10];
  }

  const changeOptionBoxTypeActiveHandler = (optionsId, activeTypeId) => {
    changeRoomOptions(room.id, optionsId, activeTypeId);
  };

  useEffect(() => {
    updateRoomCost(room.id);
  }, []);

  let contentHeight = document.documentElement.clientHeight;
  let contentWidth = document.documentElement.clientWidth;

  if (contentWidth < contentHeight) {
    contentWidth = (contentHeight * 16) / 9;
  }

  return (
    <fieldset className={styles.kitchen}>
      <div className={styles.kitchenContent}>
        <div
          className={styles.kitchenContentParts}
          style={{
            width: contentWidth,
            height: contentHeight,
          }}
        >
          <div className={styles.kitchenHeaders}>
            <h2 className="calculator-title dark">{room.name}</h2>
            <p className="calculator-subtitle dark">
              {currentLang === "ru-RU"
                ? "Выберите необходимые параметры"
                : "Виберіть необхідні параметри"}
            </p>
            <div className={styles.price}>
              {currentLang === "ru-RU" ? "Стоимость " : "Вартість "} &nbsp;
              <span className={styles.priceNum}> {cost} </span>&nbsp; грн/м
              <sup>2</sup>
            </div>
          </div>

          <img
            className={styles.basicImg}
            src="https://www.vidbydova.com.ua/assets/img/kitchen/kitchen_basic.webp"
            alt="basis kitchen"
            loading="lazy"
            style={{ width: contentWidth, height: contentHeight }}
          />

          {room.options.map((optionBox) => (
            <OptionBox
              key={optionBox.id}
              optionRoom={optionBox}
              imageStyle={{
                width: contentWidth,
                height: contentHeight,
              }}
              changeOptionBoxTypeActive={changeOptionBoxTypeActiveHandler}
            />
          ))}

          <div className={styles.kitchenBottom}>
            <button
              className="calculator-new__btn btn-prev"
              onClick={clickPrevBtn}
            >
              <span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="25"
                  height="8"
                  viewBox="0 0 25 8"
                  fill="#000"
                >
                  <path d="M0.64666 3.64645C0.451399 3.84171 0.451399 4.15829 0.64666 4.35355L3.82864 7.53553C4.0239 7.7308 4.34048 7.7308 4.53575 7.53553C4.73101 7.34027 4.73101 7.02369 4.53575 6.82843L1.70732 4L4.53575 1.17157C4.73101 0.976311 4.73101 0.659728 4.53575 0.464466C4.34048 0.269204 4.0239 0.269204 3.82864 0.464466L0.64666 3.64645ZM24.0127 3.5L1.00021 3.5V4.5L24.0127 4.5V3.5Z" />
                </svg>
                {currentLang === "ru-RU" ? "Назад" : "Назад"}
              </span>
            </button>

            <Steps steps={steps} activeStep="4" dark />

            <button
              className={classNames("calculator-new__btn btn-next", styles.btn)}
              onClick={clickNextBtn}
            >
              <span>
                {nextStep.step}
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="8"
                  viewBox="0 0 24 8"
                  fill="#fff"
                >
                  <path d="M23.366 4.35355C23.5613 4.15829 23.5613 3.84171 23.366 3.64645L20.1841 0.464466C19.9888 0.269204 19.6722 0.269204 19.4769 0.464466C19.2817 0.659728 19.2817 0.976311 19.4769 1.17157L22.3054 4L19.4769 6.82843C19.2817 7.02369 19.2817 7.34027 19.4769 7.53553C19.6722 7.7308 19.9888 7.7308 20.1841 7.53553L23.366 4.35355ZM0 4.5H23.0125V3.5H0V4.5Z" />
                </svg>
              </span>
            </button>
          </div>
        </div>
      </div>
    </fieldset>
  );
};

export default KitchenStep;
