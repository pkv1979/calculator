import classNames from "classnames";

import styles from "./Step.module.css";

const Step = (props) => {
  const { name, active, dark } = props;
  return (
    <li
      className={classNames(
        styles.step,
        active ? styles.active : "",
        dark ? "dark" : ""
      )}
    >
      <div className={styles.ellipse}></div>
      {name}
    </li>
  );
};

export default Step;
