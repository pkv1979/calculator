import { useState } from "react";

import RoomsArea from "../RoomsArea/RoomsArea";
import RoomsCount from "../RoomsCount/RoomsCount";

import styles from "./RoomsComponent.module.css";

const RoomsComponent = (props) => {
  const {
    room,
    room_id,
    maxRoom,
    rooms,
    enableAddRoom,
    changeStepVisible,
    changeVisibleRoom,
    changeRoomArea,
    currentLang,
  } = props;

  const [roomAreas, setRoomAreas] = useState([]);
  const addRoomAreaHandler = (id) => {
    const roomData = rooms.filter((room) => room.id === id);
    const roomArea = {
      id,
      area: roomData[0].area,
    };
    setRoomAreas([...roomAreas, roomArea]);
    changeStepVisible(id, true);
    changeVisibleRoom(id, true);
  };
  const deleteRoomAreaHandler = (id) => {
    setRoomAreas(roomAreas.filter((roomArea) => roomArea.id !== id));
    changeStepVisible(id, false);
    changeVisibleRoom(id, false);
  };

  return (
    <div className={styles.roomsComponent}>
      <RoomsCount
        room={room}
        room_id={room_id}
        maxRoom={maxRoom}
        currentRooms={rooms.filter((room) => room.visible).length}
        enableAddRoom={enableAddRoom}
        addRoomArea={addRoomAreaHandler}
        deleteRoomArea={deleteRoomAreaHandler}
      />
      <div className={styles.roomsAreaWrapper}>
        {rooms
          .filter((room) => room.visible)
          .map((room) => {
            return (
              <RoomsArea
                key={room.id}
                id={room.id}
                rooms={rooms}
                changeRoomArea={changeRoomArea}
                currentLang={currentLang}
              />
            );
          })}
      </div>
    </div>
  );
};

export default RoomsComponent;
