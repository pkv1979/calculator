import { useEffect } from "react";

import styles from "./BathroomStep.module.css";
import OptionBox from "../OptionBox/OptionBox";
import Steps from "../Steps";
import classNames from "classnames";

const BathroomStep = (props) => {
  const {
    steps,
    room,
    cost,
    changeActiveStep,
    changeRoomOptions,
    updateRoomCost,
    currentLang,
  } = props;

  const clickNextBtn = () => {
    changeActiveStep(nextStep.id);
  };

  const clickPrevBtn = () => {
    changeActiveStep(prevStep.id);
  };

  let prevStep = {};
  if (steps[6].visible) {
    prevStep = steps[6];
  } else if (steps[5].visible) {
    prevStep = steps[5];
  } else if (steps[4].visible) {
    prevStep = steps[4];
  } else if (steps[3].visible) {
    prevStep = steps[3];
  } else {
    prevStep = steps[2];
  }

  let nextStep = {};
  if (steps[8].visible) {
    nextStep = steps[8];
  } else if (steps[9].visible) {
    nextStep = steps[9];
  } else {
    nextStep = steps[10];
  }

  const changeOptionBoxTypeActiveHandler = (optionsId, activeTypeId) => {
    changeRoomOptions(room.id, optionsId, activeTypeId);
  };

  useEffect(() => {
    updateRoomCost(room.id);
  }, []);

  let contentWidth = window.innerWidth;
  let contentHeight = window.innerHeight;

  if (contentWidth < contentHeight) {
    contentWidth = (contentHeight * 16) / 9;
  }

  return (
    <fieldset className={styles.bathroom}>
      <div className={styles.bathroomContent}>
        <div
          className={styles.bathroomContentParts}
          style={{
            width: contentWidth,
            height: contentHeight,
          }}
        >
          <div className={styles.bathroomHeaders}>
            <h2 className="calculator-title dark mobileLight">{room.name}</h2>
            <p className="calculator-subtitle dark mobileLight">
              {currentLang === "ru-RU"
                ? "Выберите необходимые параметры"
                : "Виберіть необхідні параметри"}
            </p>
            <div className={styles.price}>
              {currentLang === "ru-RU" ? "Стоимость" : "Вартість"}&nbsp;
              <span className={styles.priceNum}> {cost} </span>&nbsp; грн/м
              <sup>2</sup>
            </div>
          </div>

          <img
            className={styles.basicImg}
            src="https://www.vidbydova.com.ua/assets/img/bathroom/bathroom_basic.webp"
            alt="bathroom basic image"
            loading="lazy"
            style={{ width: contentWidth, height: contentHeight }}
          />

          {room.options.map((option) => (
            <OptionBox
              key={option.id}
              optionRoom={option}
              changeOptionBoxTypeActive={changeOptionBoxTypeActiveHandler}
              imageStyle={{
                width: contentWidth,
                height: contentHeight,
              }}
            />
          ))}

          <div className={styles.bathroomBottom}>
            <button
              className="calculator-new__btn btn-prev"
              onClick={clickPrevBtn}
            >
              <span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="25"
                  height="8"
                  viewBox="0 0 25 8"
                  fill="#000"
                >
                  <path d="M0.64666 3.64645C0.451399 3.84171 0.451399 4.15829 0.64666 4.35355L3.82864 7.53553C4.0239 7.7308 4.34048 7.7308 4.53575 7.53553C4.73101 7.34027 4.73101 7.02369 4.53575 6.82843L1.70732 4L4.53575 1.17157C4.73101 0.976311 4.73101 0.659728 4.53575 0.464466C4.34048 0.269204 4.0239 0.269204 3.82864 0.464466L0.64666 3.64645ZM24.0127 3.5L1.00021 3.5V4.5L24.0127 4.5V3.5Z" />
                </svg>
                {currentLang === "ru-RU" ? "Назад" : "Назад"}
              </span>
            </button>

            <Steps steps={steps} activeStep="8" dark />

            <button
              className={classNames("calculator-new__btn btn-next", styles.btn)}
              onClick={clickNextBtn}
            >
              <span>
                {nextStep.step}
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="8"
                  viewBox="0 0 24 8"
                  fill="#fff"
                >
                  <path d="M23.366 4.35355C23.5613 4.15829 23.5613 3.84171 23.366 3.64645L20.1841 0.464466C19.9888 0.269204 19.6722 0.269204 19.4769 0.464466C19.2817 0.659728 19.2817 0.976311 19.4769 1.17157L22.3054 4L19.4769 6.82843C19.2817 7.02369 19.2817 7.34027 19.4769 7.53553C19.6722 7.7308 19.9888 7.7308 20.1841 7.53553L23.366 4.35355ZM0 4.5H23.0125V3.5H0V4.5Z" />
                </svg>
              </span>
            </button>
          </div>
        </div>
      </div>
    </fieldset>
  );
};

export default BathroomStep;
