import Step from "../Step/Step";
import styles from "./Steps.module.css";

const Steps = (props) => {
  const { steps, activeStep, dark = false } = props;
  return (
    <ul className={styles.steps}>
      {steps
        .filter((step) => step.visible)
        .map((step) => {
          return (
            <Step
              key={step.id}
              name={step.step}
              active={activeStep == step.id}
              dark={dark}
            />
          );
        })}
    </ul>
  );
};

export default Steps;
