import classNames from "classnames";

import Steps from "../Steps";

import styles from "./ConditionStep.module.css";
import { useState } from "react";

const ConditionStep = (props) => {
  const { updateInfo, steps, changeActiveStep, currentLang } = props;

  const clickNextBtn = () => {
    changeActiveStep(nextStep.id);
  };

  const clickPrevBtn = () => {
    changeActiveStep(2);
  };

  let nextStep = {};
  if (steps[3].visible) {
    nextStep = steps[3];
  } else if (steps[4].visible) {
    nextStep = steps[4];
  } else if (steps[7].visible) {
    nextStep = steps[7];
  } else if (steps[9].visible) {
    nextStep = steps[9];
  } else {
    nextStep = steps[10];
  }

  const [isWallsRight, setIsWallsRight] = useState(false);
  const toggleWallsClass = () => {
    updateInfo(
      "conditionWalls",
      isWallsRight
        ? currentLang === "ru-RU"
          ? "Голые стены"
          : "Голі стіни"
        : currentLang === "ru-RU"
        ? "Штукатуренные стены"
        : "Штукатурені стіни"
    );
    setIsWallsRight(!isWallsRight);
  };

  const [isFloorsRight, setIsFloorsRight] = useState(true);
  const toggleFloorsClass = () => {
    updateInfo(
      "conditionFloors",
      isFloorsRight
        ? currentLang === "ru-RU"
          ? "Полы со стяжкой"
          : "Підлога зі стяжкою"
        : currentLang === "ru-RU"
        ? "Полы без стяжки"
        : "Підлога без стяжки"
    );
    setIsFloorsRight(!isFloorsRight);
  };

  return (
    <div
      id="condition"
      className={classNames(styles.calculatorCondition, "calculatorFieldset")}
    >
      <div className="content">
        <h2 className="calculator-title">
          {currentLang === "ru-RU" ? "Состояние помещений" : "Стан приміщень"}
        </h2>
        <p className="calculator-subtitle">
          {currentLang === "ru-RU"
            ? "Выберите состояние на данный момент"
            : "Виберіть стан на даний момент"}
        </p>

        <div
          className={classNames("calculator-wrapper", styles.conditionWrapper)}
        >
          <div className={styles.conditionWallsFloors}>
            <div className={styles.conditionWalls}>
              <img
                src="https://www.vidbydova.com.ua/assets/img/bare_walls.avif"
                alt={currentLang === "ru-RU" ? "Голые стены" : "Голі стіни"}
                // width="420"
                // height="279"
                className={classNames(
                  styles.conditionImg,
                  isWallsRight ? "" : styles.active
                )}
              />
              <img
                src="https://www.vidbydova.com.ua/assets/img/plastered_walls.avif"
                alt={
                  currentLang === "ru-RU"
                    ? "Штукатуренные стены"
                    : "Штукатурені стіни"
                }
                // width="420"
                // height="279"
                className={classNames(
                  styles.conditionImg,
                  isWallsRight ? styles.active : ""
                )}
              />

              <div className={styles.conditionWallsMiddle}>
                <p className={isWallsRight ? "" : styles.active}>
                  {currentLang === "ru-RU" ? "Голые стены" : "Голі стіни"}
                </p>
                <div className={styles.conditionSwitcher}>
                  <div
                    className={styles.conditionSwitch}
                    onClick={toggleWallsClass}
                  >
                    <div className={styles.conditionSwitchOne}></div>
                    <div
                      className={classNames(
                        styles.conditionSwitchToggle,
                        isWallsRight ? styles.right : styles.left
                      )}
                    ></div>
                    <div className={styles.conditionSwitchUno}></div>
                  </div>
                </div>
                <p
                  className={classNames(
                    styles.textRight,
                    isWallsRight ? styles.active : ""
                  )}
                >
                  {currentLang === "ru-RU"
                    ? "Штукатуренные стены"
                    : "Штукатурені стіни"}
                </p>
              </div>

              <div className={styles.conditionWallsBottom}>
                <p>
                  *
                  {currentLang === "ru-RU"
                    ? "нужен комплекс штукатурных работ"
                    : "потрібен комплекс штукатурних робіт"}
                </p>
                <p className={styles.textRight}>
                  *
                  {currentLang === "ru-RU"
                    ? "штукатурные работы уже проведены"
                    : "штукатурні роботи вже проведено"}
                </p>
              </div>
            </div>

            <div className={styles.conditionFloors}>
              <img
                src="https://www.vidbydova.com.ua/assets/img/screeded_floors.avif"
                alt={
                  currentLang === "ru-RU"
                    ? "Полы со стяжкой"
                    : "Підлога зі стяжкою"
                }
                // width="420"
                // height="279"
                className={classNames(
                  styles.conditionImg,
                  isFloorsRight ? "" : styles.active
                )}
              />
              <img
                src="https://www.vidbydova.com.ua/assets/img/floors_without_screed.avif"
                alt={
                  currentLang === "ru-RU"
                    ? "Полы без стяжки"
                    : "Підлога без стяжки"
                }
                // width="420"
                // height="279"
                className={classNames(
                  styles.conditionImg,
                  isFloorsRight ? styles.active : ""
                )}
              />

              <div className={styles.conditionFloorsMiddle}>
                <p className={isFloorsRight ? "" : styles.active}>
                  {currentLang === "ru-RU"
                    ? "Полы со стяжкой"
                    : "Підлога зі стяжкою"}
                </p>
                <div className={styles.conditionSwitcher}>
                  <div
                    className={styles.conditionSwitch}
                    onClick={toggleFloorsClass}
                  >
                    <div className={styles.conditionSwitchOne}></div>
                    <div
                      className={classNames(
                        styles.conditionSwitchToggle,
                        isFloorsRight ? styles.right : styles.left
                      )}
                    ></div>
                    <div className={styles.conditionSwitchUno}></div>
                  </div>
                </div>
                <p
                  className={classNames(
                    styles.textRight,
                    isFloorsRight ? styles.active : ""
                  )}
                >
                  {currentLang === "ru-RU"
                    ? "Полы без стяжки"
                    : "Підлога без стяжки"}
                </p>
              </div>

              <div className={styles.conditionFloorsBottom}>
                <p>
                  *
                  {currentLang === "ru-RU"
                    ? "полы готовы к ремонту"
                    : "підлога готова до ремонту"}
                </p>
                <p className={styles.textRight}>
                  *
                  {currentLang === "ru-RU"
                    ? "нужен комплекс работ по стяжке пола"
                    : "потрібен комплекс робіт зі стяжки підлоги"}
                </p>
              </div>
            </div>
          </div>

          <div className={styles.buttonWrapper}></div>
        </div>
      </div>
      <div className="calculator-form-bottom">
        <button className="calculator-new__btn btn-prev" onClick={clickPrevBtn}>
          <span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="25"
              height="8"
              viewBox="0 0 25 8"
              fill="#000"
            >
              <path d="M0.64666 3.64645C0.451399 3.84171 0.451399 4.15829 0.64666 4.35355L3.82864 7.53553C4.0239 7.7308 4.34048 7.7308 4.53575 7.53553C4.73101 7.34027 4.73101 7.02369 4.53575 6.82843L1.70732 4L4.53575 1.17157C4.73101 0.976311 4.73101 0.659728 4.53575 0.464466C4.34048 0.269204 4.0239 0.269204 3.82864 0.464466L0.64666 3.64645ZM24.0127 3.5L1.00021 3.5V4.5L24.0127 4.5V3.5Z" />
            </svg>
            {currentLang === "ru-RU" ? "Назад" : "Назад"}
          </span>
        </button>

        <Steps steps={steps} activeStep="3" />

        <button
          className={classNames("calculator-new__btn btn-next", styles.btn)}
          onClick={clickNextBtn}
        >
          <span>
            {nextStep.step}
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="8"
              viewBox="0 0 24 8"
              fill="#fff"
            >
              <path d="M23.366 4.35355C23.5613 4.15829 23.5613 3.84171 23.366 3.64645L20.1841 0.464466C19.9888 0.269204 19.6722 0.269204 19.4769 0.464466C19.2817 0.659728 19.2817 0.976311 19.4769 1.17157L22.3054 4L19.4769 6.82843C19.2817 7.02369 19.2817 7.34027 19.4769 7.53553C19.6722 7.7308 19.9888 7.7308 20.1841 7.53553L23.366 4.35355ZM0 4.5H23.0125V3.5H0V4.5Z" />
            </svg>
          </span>
        </button>
      </div>
    </div>
  );
};

export default ConditionStep;
