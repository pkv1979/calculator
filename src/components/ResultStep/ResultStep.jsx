import { useEffect, useState } from "react";
import classNames from "classnames";
import ReactInputMask from "react-input-mask";

import { Autoplay, Navigation } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";

import styles from "./ResultStep.module.css";

import Steps from "../Steps";

const ResultStep = (props) => {
  const {
    phone,
    updateInfo,
    totalCost,
    apartmentsArea,
    steps,
    changeActiveStep,
    messenger,
    currentLang,
  } = props;

  const [slides, setSlides] = useState([]);
  const [isSubmitButtonActive, setIsSubmitButtonActive] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    fetch("https://www.vidbydova.com.ua/wp-json/wp/v2/calculator-portfolio")
      .then((response) => response.json())
      .then((json) => setSlides(json));
  }, []);

  const clickPrevBtn = () => {
    changeActiveStep(prevStep.id);
  };

  let prevStep = {};
  if (steps[10].visible) {
    prevStep = steps[10];
  } else if (steps[9].visible) {
    prevStep = steps[9];
  } else if (steps[8].visible) {
    prevStep = steps[8];
  } else if (steps[7].visible) {
    prevStep = steps[7];
  } else if (steps[6].visible) {
    prevStep = steps[6];
  } else if (steps[5].visible) {
    prevStep = steps[5];
  } else if (steps[4].visible) {
    prevStep = steps[4];
  } else if (steps[3].visible) {
    prevStep = steps[3];
  } else {
    prevStep = steps[2];
  }

  const updatePhone = (e) => {
    const newPhone = e.target.value;

    updateInfo("phone", newPhone);

    //Проверка валидности введенного номера телефона
    const isValidPhone = /^\+380\(\d{2}\)\d{3}-\d{2}-\d{2}$/.test(newPhone);
    setIsSubmitButtonActive(isValidPhone);

    // Установка текста ошибки (пример)
    if (!isValidPhone) {
      setErrorMessage(
        currentLang === "ru-RU"
          ? "Введите корректный номер телефона"
          : "Введіть коректний номер телефону"
      );
    } else {
      setErrorMessage("");
    }
  };

  const changeMessenger = (e) => {
    updateInfo("messenger", e.target.alt);
  };

  const slidesPerView = window.innerWidth > 426 ? 2 : 1;

  return (
    <div className={classNames(styles.calculatorResult, "calculatorFieldset")}>
      <div className="content">
        <div className={styles.resultWrapper}>
          <div className={styles.resultLeft}>
            <h2 className={styles.resultTitle}>
              {currentLang === "ru-RU"
                ? "Скачать детальную смету"
                : "Завантажити детальний кошторис"}
            </h2>
            <div className={styles.inputBox}>
              <ReactInputMask
                className={styles.phoneInput}
                type="tel"
                mask="+380(99)999-99-99"
                maskChar="_"
                value={phone}
                placeholder={
                  currentLang === "ru-RU" ? "Номер телефона" : "Номер телефону"
                }
                required
                onChange={updatePhone}
              />
              {errorMessage && (
                <p className={styles.errorMessage}>{errorMessage}</p>
              )}
            </div>
            <p className={styles.subText}>
              {currentLang === "ru-RU"
                ? "Выберите где вам удобнее получить смету"
                : "Оберіть де вам зручніше отримати кошторис"}
            </p>
            <div className={styles.messengers}>
              <img
                className={classNames(
                  styles.messengerImg,
                  messenger === "viber" ? styles.select : ""
                )}
                src="https://www.vidbydova.com.ua/assets/img/viber.svg"
                alt="viber"
                onClick={changeMessenger}
              />
              <img
                className={classNames(
                  styles.messengerImg,
                  messenger === "telegram" ? styles.select : ""
                )}
                src="https://www.vidbydova.com.ua/assets/img/telegram.svg"
                alt="telegram"
                onClick={changeMessenger}
              />
            </div>
            <button
              className={styles.submitBtn}
              type="submit"
              disabled={!isSubmitButtonActive}
            >
              {currentLang === "ru-RU" ? "Получить смету" : "Отримати кошторис"}
            </button>
          </div>

          <div className={styles.resultRight}>
            <div className={styles.resultRightTop}>
              <div className={styles.resultRow}>
                <div className={styles.resultText}>
                  {currentLang === "ru-RU"
                    ? "Стоимость ремонта"
                    : "Вартість ремонту"}
                </div>
                <div className={styles.resultValue}>{totalCost.cost} грн</div>
              </div>
              <div className={styles.resultRow}>
                <div className={styles.resultText}>
                  {currentLang === "ru-RU"
                    ? "Стоимость за м.кв."
                    : "Вартість за м.кв."}
                </div>
                <div className={styles.resultValue}>
                  {totalCost.cost_for_square_meter} грн
                </div>
              </div>
              <div className={styles.resultRow}>
                <div className={styles.resultText}>
                  {currentLang === "ru-RU"
                    ? "Приблизительные сроки"
                    : "Приблизні строки"}
                </div>
                <div className={styles.resultValue}>
                  {apartmentsArea <= 80 ? 4 : 5}{" "}
                  {currentLang === "ru-RU" ? "мес" : "міс"}
                </div>
              </div>
            </div>

            {/* <div className={styles.resultRightBottom}>
              <p className={styles.sliderTitle}>
                {currentLang === "ru-RU"
                  ? "Работы, которые мы уже выполнили"
                  : "Роботи, які ми вже виконали"}
              </p>
              <Swiper
                modules={[Autoplay, Navigation]}
                spaceBetween={0}
                slidesPerView={slidesPerView}
                navigation
                autoplay={{ delay: 5000 }}
              >
                {slides.map((slide, index) => (
                  <SwiperSlide key={index} className={styles.slide}>
                    <a href={slide.url} className={styles.slideMainLink}></a>
                    <img
                      className={styles.sildeImg}
                      src={slide.imageUrl}
                      alt={slide.text}
                    />
                    <div className={styles.slideBottom}>
                      <a href={styles.url} className={styles.slideBtn}>
                        <span className={styles.slideBtnArrow}></span>
                      </a>
                      <div className={styles.slideName}>
                        <a href={slide.url}>{slide.text}</a>
                      </div>
                    </div>
                  </SwiperSlide>
                ))}
              </Swiper>
            </div> */}
          </div>
        </div>
      </div>

      <div className="calculator-form-bottom">
        <button
          className={classNames(
            "calculator-new__btn btn-prev",
            styles.resultBtn
          )}
          onClick={clickPrevBtn}
        >
          <span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="25"
              height="8"
              viewBox="0 0 25 8"
              fill="#000"
            >
              <path d="M0.64666 3.64645C0.451399 3.84171 0.451399 4.15829 0.64666 4.35355L3.82864 7.53553C4.0239 7.7308 4.34048 7.7308 4.53575 7.53553C4.73101 7.34027 4.73101 7.02369 4.53575 6.82843L1.70732 4L4.53575 1.17157C4.73101 0.976311 4.73101 0.659728 4.53575 0.464466C4.34048 0.269204 4.0239 0.269204 3.82864 0.464466L0.64666 3.64645ZM24.0127 3.5L1.00021 3.5V4.5L24.0127 4.5V3.5Z" />
            </svg>
            {currentLang === "ru-RU" ? "Назад" : "Назад"}
          </span>
        </button>

        <Steps steps={steps} activeStep="12" />

        <div></div>
      </div>
    </div>
  );
};

export default ResultStep;
