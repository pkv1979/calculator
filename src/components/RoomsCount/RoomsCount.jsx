import { useState } from "react";
import classNames from "classnames";

import styles from "./RoomsCount.module.css";

const RoomsCount = (props) => {
  const {
    room,
    room_id,
    maxRoom,
    currentRooms,
    enableAddRoom,
    addRoomArea,
    deleteRoomArea,
  } = props;

  const [count, setCount] = useState(currentRooms);

  const addRoom = () => {
    let enableAdd = true;
    if (enableAddRoom !== undefined && enableAddRoom[0].visible) {
      enableAdd = false;
    }
    if (count < maxRoom && enableAdd) {
      addRoomArea(room_id + "_" + (count + 1), room_id);
      setCount(count + 1);
    }
  };

  const subRoom = () => {
    if (count > 0) {
      deleteRoomArea(room_id + "_" + count);
      setCount(count - 1);
    }
  };

  return (
    <div className={classNames("roomsBlock", styles.roomsNumbers)}>
      <label className={styles.roomsLabel}>{room}</label>
      <div className={styles.roomsNumber}>
        <button type="button" className={styles.minus} onClick={subRoom}>
          -
        </button>
        <div className={styles.roomsCount}>{count}</div>
        <button type="button" className={styles.plus} onClick={addRoom}>
          +
        </button>
      </div>
    </div>
  );
};

export default RoomsCount;
