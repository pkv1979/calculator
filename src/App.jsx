import { useState } from "react";
import Swal from "sweetalert2";

import "./App.css";

import ParametersStep from "./components/ParametersStep";
import ConditionStep from "./components/ConditionStep/ConditionStep";
import KitchenStep from "./components/KitchenStep/KitchenStep";
import RoomStep from "./components/RoomStep/RoomStep";
import RoomsStep from "./components/RoomsStep";
import Room2Step from "./components/Room2Step";
import Room3Step from "./components/Room3Step";
import BathroomStep from "./components/BathroomStep";
import Bathroom2Step from "./components/Bathroom2Step";
import StudioStep from "./components/StudioStep";
import AdditionalOptionsStep from "./components/AdditionalOptionsStep";
import ResultStep from "./components/ResultStep";

function App() {
  const currentLang = document.documentElement.getAttribute("lang");
  const [rooms, setRooms] = useState([
    {
      id: "kitchen_1",
      name: currentLang === "ru-RU" ? "Кухня" : "Кухня",
      area: 0,
      amount_meter: 0,
      amount: 0,
      visible: false,
      options: [
        {
          id: "walls",
          name: currentLang === "ru-RU" ? "Стены" : "Стіни",
          style: {
            right: "10%",
            top: "30%",
          },
          types: [
            {
              id: "wallpaper",
              name: currentLang === "ru-RU" ? "Обои" : "Шпалери",
              price: 480,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/walls/wallpaper.webp",
              },
            },
            {
              id: "painting",
              name: currentLang === "ru-RU" ? "Покраска" : "Фарбування",
              price: 570,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/walls/painting.webp",
              },
            },
            {
              id: "decorative_plaster",
              name:
                currentLang === "ru-RU"
                  ? "Декоративная штукатурка"
                  : "Декоративна штукатурка",
              price: 930,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/walls/decorative_plaster.webp",
              },
            },
          ],
        },
        {
          id: "doors",
          name: currentLang === "ru-RU" ? "Двери" : "Двері",
          style: {
            right: "5%",
            bottom: "35%",
            zIndex: "6",
          },
          listStyle: {
            right: 0,
            left: "auto",
          },
          types: [
            {
              id: "flush_mounted_doors",
              name:
                currentLang === "ru-RU"
                  ? "Двери скрытого монтажа"
                  : "Двері прихованого монтажу",
              price: 2850,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/doors/flush_mounted_doors.webp",
              },
            },
            {
              id: "door_with_platbands",
              name:
                currentLang === "ru-RU"
                  ? "Дверь с наличниками"
                  : "Двері з лиштвами",
              price: 2400,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/doors/door_with_platbands.webp",
              },
            },
          ],
        },
        {
          id: "skirting",
          name: currentLang === "ru-RU" ? "Плинтус" : "Плінтус",
          style: {
            right: "18%",
            bottom: "16%",
            zIndex: "5",
          },
          listStyle: {
            bottom: "40px",
            top: "auto",
            right: 0,
            left: "auto",
          },
          types: [
            {
              id: "plastic",
              name: currentLang === "ru-RU" ? "Пластиковый" : "Пластиковий",
              price: 80,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/skirting/plastic.webp",
              },
            },
            {
              id: "mdf_skirting_board",
              name: currentLang === "ru-RU" ? "мдф плинтус" : "мдф плінтус",
              price: 140,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/skirting/mdf_skirting_board.webp",
              },
            },
            {
              id: "flush_skirting_board",
              name:
                currentLang === "ru-RU"
                  ? "плинтус скрытого монтажа"
                  : "плінтус прихованого монтажу",
              price: 260,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/skirting/flush_skirting_board.webp",
              },
            },
          ],
        },
        {
          id: "ceiling",
          name: currentLang === "ru-RU" ? "Потолок" : "Стеля",
          style: {
            right: "15%",
            top: "5%",
          },
          types: [
            {
              id: "stretch_ceiling",
              name:
                currentLang === "ru-RU" ? "Натяжной потолок" : "Натяжна стеля",
              price: 450,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/ceiling/stretch_ceiling.webp",
              },
            },
            {
              id: "painted_ceiling",
              name:
                currentLang === "ru-RU"
                  ? "Потолок под покраску"
                  : "Стеля під фарбування",
              price: 480,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/ceiling/painted_ceiling.webp",
              },
            },
            {
              id: "plaster_ceiling",
              name: currentLang === "ru-RU" ? "Потолок гипс" : "Стеля гіпс",
              price: 750,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/ceiling/plaster_ceiling.webp",
              },
            },
          ],
        },
        {
          id: "lighting",
          name: currentLang === "ru-RU" ? "Освещение" : "Освітлення",
          style: {
            left: "20%",
            top: "5%",
          },
          types: [
            {
              id: "chandelier",
              name: currentLang === "ru-RU" ? "Люстра" : "Люстра",
              unitOfMeasurement: "шт",
              isMeter: false,
              price: 800,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/lighting/chandelier.webp",
              },
            },
            {
              id: "track",
              name: currentLang === "ru-RU" ? "трековые" : "трекові",
              price: 360,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/lighting/track.webp",
              },
            },
            {
              id: "point_illumination",
              name:
                currentLang === "ru-RU"
                  ? "освещение точки"
                  : "освітлення точки",
              price: 200,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/lighting/point_illumination.webp",
              },
            },
          ],
        },
        {
          id: "windows",
          name: currentLang === "ru-RU" ? "Окна" : "Вікна",
          style: {
            left: "5%",
            top: "40%",
          },
          types: [
            {
              id: "need_to_change",
              name: currentLang === "ru-RU" ? "надо менять" : "треба міняти",
              price: 1000,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
            },
            {
              id: "do_not_change",
              name:
                currentLang === "ru-RU" ? "не надо менять" : "не треба міняти",
              price: 0,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
            },
          ],
        },
        {
          id: "floor",
          name: currentLang === "ru-RU" ? "Пол" : "Підлога",
          style: {
            left: "23%",
            bottom: "15%",
          },
          listStyle: {
            bottom: "40px",
            top: "auto",
          },
          types: [
            {
              id: "laminate",
              name: currentLang === "ru-RU" ? "Ламинат" : "Ламінат",
              price: 260,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/floor/laminate.webp",
              },
            },
            {
              id: "quartz_vinyl",
              name: currentLang === "ru-RU" ? "кварц винил" : "кварц вініл",
              price: 400,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/floor/quartz_vinyl.webp",
              },
            },
            {
              id: "parquet_flooring",
              name:
                currentLang === "ru-RU" ? "паркетная доска" : "паркетна дошка",
              price: 485,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/floor/parquet_flooring.webp",
              },
            },
            {
              id: "tile",
              name:
                currentLang === "ru-RU"
                  ? "плитка (стандарт)"
                  : "плитка (стандарт)",
              price: 490,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/floor/tile.webp",
              },
            },
          ],
        },
        {
          id: "apron",
          name: currentLang === "ru-RU" ? "Фартук" : "Фартух",
          style: {
            left: "50%",
            top: "50%",
          },
          listStyle: {
            bottom: "40px",
            top: "auto",
          },
          types: [
            {
              id: "ceramic_tile_apron",
              name:
                currentLang === "ru-RU"
                  ? "Фартук из керамической плитки"
                  : "Фартух із керамічної плитки",
              price: 550,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/apron/ceramic_tile_apron.webp",
              },
            },
            {
              id: "apron_made_of_mdf_panels",
              name:
                currentLang === "ru-RU"
                  ? "Фартук из МДФ панелей"
                  : "Фартух із МДФ панелей",
              price: 2200,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/apron/apron_made_of_mdf_panels.webp",
              },
            },
            {
              id: "artificial_stone_apron",
              name:
                currentLang === "ru-RU"
                  ? "Фартук из искусственного камня"
                  : "Фартух зі штучного каменю",
              price: 3500,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/apron/artificial_stone_apron.webp",
              },
            },
            {
              id: "skenali_glass_aprons",
              name:
                currentLang === "ru-RU"
                  ? "Стеклянные фартуки скенали"
                  : "Скляні фартухи скеналі",
              price: 2000,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/apron/skenali_glass_aprons.webp",
              },
            },
            {
              id: "wall_panels",
              name:
                currentLang === "ru-RU" ? "стеновые панели" : "стінові панелі",
              price: 2000,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/apron/wall_panels.webp",
              },
            },
          ],
        },
      ],
    },
    {
      id: "room_1",
      name: currentLang === "ru-RU" ? "Комната 1" : "Кімната 1",
      area: 0,
      amount_meter: 0,
      amount: 0,
      visible: false,
      options: [
        {
          id: "floor",
          name: currentLang === "ru-RU" ? "Пол" : "Підлога",
          style: {
            left: "30%",
            bottom: "17%",
          },
          listStyle: {
            bottom: "40px",
            top: "auto",
          },
          types: [
            {
              id: "laminate",
              name: currentLang === "ru-RU" ? "Ламинат" : "Ламінат",
              price: 260,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/floor/laminate.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "quartz_vinyl",
              name: currentLang === "ru-RU" ? "кварц винил" : "кварц вініл",
              price: 400,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/floor/quartz_vinyl.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "parquet_flooring",
              name:
                currentLang === "ru-RU" ? "паркетная доска" : "паркетна дошка",
              price: 485,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/floor/parquet_flooring.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "tile",
              name:
                currentLang === "ru-RU"
                  ? "плитка (стандарт)"
                  : "плитка (стандарт)",
              price: 490,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/floor/tile.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
        {
          id: "walls",
          name: currentLang === "ru-RU" ? "Стены" : "Стіни",
          style: {
            right: "15%",
            top: "20%",
          },
          types: [
            {
              id: "wallpaper",
              name: currentLang === "ru-RU" ? "Обои" : "Шпалери",
              price: 480,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/walls/wallpaper.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "painting",
              name: currentLang === "ru-RU" ? "Покраска" : "Фарбування",
              price: 570,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/walls/painting.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "decorative_plaster",
              name:
                currentLang === "ru-RU"
                  ? "Декоративная штукатурка"
                  : "Декоративна штукатурка",
              price: 930,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/walls/decorative_plaster.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
        {
          id: "plinths",
          name: currentLang === "ru-RU" ? "Плинтуса" : "Плінтуси",
          style: {
            right: "20%",
            bottom: "20%",
          },
          listStyle: {
            bottom: "40px",
            top: "auto",
          },
          types: [
            {
              id: "plastic",
              name: currentLang === "ru-RU" ? "Пластиковый" : "Пластиковий",
              price: 60,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/plinths/plastic.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "mdf_plinth",
              name: currentLang === "ru-RU" ? "мдф плинтус" : "мдф плінтус",
              price: 135,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/plinths/mdf_plinth.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "flush_skirting_board",
              name:
                currentLang === "ru-RU"
                  ? "плинтус скрытого монтажа"
                  : "плінтус прихованого монтажу",
              price: 250,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/plinths/flush_skirting_board.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
        {
          id: "doors",
          name: currentLang === "ru-RU" ? "Двери" : "Двері",
          style: {
            right: "5%",
            bottom: "45%",
          },
          listStyle: {
            right: "0",
            left: "auto",
          },
          types: [
            {
              id: "flush_mounted_doors",
              name:
                currentLang === "ru-RU"
                  ? "Двери скрытого монтажа"
                  : "Двері прихованого монтажу",
              price: 2850,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/doors/flush_mounted_doors.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "door_with_platbands",
              name:
                currentLang === "ru-RU"
                  ? "Дверь с наличниками"
                  : "Двері з лиштвами",
              price: 2400,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/doors/door_with_platbands.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
        {
          id: "windows",
          name: currentLang === "ru-RU" ? "Окна" : "Вікна",
          style: {
            left: "10%",
            top: "40%",
          },
          types: [
            {
              id: "need_to_change",
              name: currentLang === "ru-RU" ? "надо менять" : "треба міняти",
              price: 1000,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
            },
            {
              id: "do_not_need_to_change",
              name:
                currentLang === "ru-RU" ? "не надо менять" : "не треба міняти",
              price: 0,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
            },
          ],
        },
        {
          id: "ceiling",
          name: currentLang === "ru-RU" ? "Потолок" : "Стеля",
          style: {
            right: "20%",
            top: "5%",
          },
          types: [
            {
              id: "stretch_ceiling",
              name:
                currentLang === "ru-RU" ? "Натяжной потолок" : "Натяжна стеля",
              price: 450,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/ceiling/stretch_ceiling.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "painted_ceiling",
              name:
                currentLang === "ru-RU"
                  ? "Потолок под покраску"
                  : "Стеля під фарбування",
              price: 480,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/ceiling/painted_ceiling.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "plaster_ceiling",
              name: currentLang === "ru-RU" ? "Потолок гипс" : "Стеля гіпс",
              price: 750,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/ceiling/plaster_ceiling.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
        {
          id: "lighting",
          name: currentLang === "ru-RU" ? "Освещение" : "Освітлення",
          style: {
            left: "23%",
            top: "7%",
          },
          unitOfMeasurement: "шт",
          types: [
            {
              id: "chandelier",
              name: currentLang === "ru-RU" ? "Люстра" : "Люстра",
              price: 800,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/lighting/chandelier.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                  zIndex: "1",
                },
              },
            },
            {
              id: "track",
              name: currentLang === "ru-RU" ? "трековые" : "трекові",
              price: 360,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/lighting/track.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "point",
              name: currentLang === "ru-RU" ? "точненные" : "точкові",
              price: 240,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/lighting/point.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
      ],
    },
    {
      id: "room_2",
      name: currentLang === "ru-RU" ? "Комната 2" : "Кімната 2",
      area: 0,
      amount_meter: 0,
      amount: 0,
      visible: false,
      options: [
        {
          id: "floor",
          name: currentLang === "ru-RU" ? "Пол" : "Підлога",
          style: {
            left: "30%",
            bottom: "17%",
          },
          listStyle: {
            bottom: "40px",
            top: "auto",
          },
          types: [
            {
              id: "laminate",
              name: currentLang === "ru-RU" ? "Ламинат" : "Ламінат",
              price: 260,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/floor/laminate.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "quartz_vinyl",
              name: currentLang === "ru-RU" ? "кварц винил" : "кварц вініл",
              price: 400,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/floor/quartz_vinyl.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "parquet_flooring",
              name:
                currentLang === "ru-RU" ? "паркетная доска" : "паркетна дошка",
              price: 485,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/floor/parquet_flooring.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "tile",
              name:
                currentLang === "ru-RU"
                  ? "плитка (стандарт)"
                  : "плитка (стандарт)",
              price: 490,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/floor/tile.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
        {
          id: "walls",
          name: currentLang === "ru-RU" ? "Стены" : "Стіни",
          style: {
            right: "15%",
            top: "20%",
          },
          types: [
            {
              id: "wallpaper",
              name: currentLang === "ru-RU" ? "Обои" : "Шпалери",
              price: 480,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/walls/wallpaper.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "painting",
              name: currentLang === "ru-RU" ? "Покраска" : "Фарбування",
              price: 570,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/walls/painting.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "decorative_plaster",
              name:
                currentLang === "ru-RU"
                  ? "Декоративная штукатурка"
                  : "Декоративна штукатурка",
              price: 930,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/walls/decorative_plaster.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
        {
          id: "plinths",
          name: currentLang === "ru-RU" ? "Плинтуса" : "Плінтуси",
          style: {
            right: "20%",
            bottom: "20%",
          },
          listStyle: {
            bottom: "40px",
            top: "auto",
          },
          types: [
            {
              id: "plastic",
              name: currentLang === "ru-RU" ? "Пластиковый" : "Пластиковий",
              price: 60,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/plinths/plastic.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "mdf_plinth",
              name: currentLang === "ru-RU" ? "мдф плинтус" : "мдф плінтус",
              price: 135,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/plinths/mdf_plinth.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "flush_skirting_board",
              name:
                currentLang === "ru-RU"
                  ? "плинтус скрытого монтажа"
                  : "плінтус прихованого монтажу",
              price: 250,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/plinths/flush_skirting_board.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
        {
          id: "doors",
          name: currentLang === "ru-RU" ? "Двери" : "Двері",
          style: {
            right: "5%",
            bottom: "45%",
          },
          listStyle: {
            right: "0",
            left: "auto",
          },
          types: [
            {
              id: "flush_mounted_doors",
              name:
                currentLang === "ru-RU"
                  ? "Двери скрытого монтажа"
                  : "Двері прихованого монтажу",
              price: 2850,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/doors/flush_mounted_doors.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "door_with_platbands",
              name:
                currentLang === "ru-RU"
                  ? "Дверь с наличниками"
                  : "Двері з лиштвами",
              price: 2400,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/doors/door_with_platbands.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
        {
          id: "windows",
          name: currentLang === "ru-RU" ? "Окна" : "Вікна",
          style: {
            left: "10%",
            top: "40%",
          },
          types: [
            {
              id: "need_to_change",
              name: currentLang === "ru-RU" ? "надо менять" : "треба міняти",
              price: 1000,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
            },
            {
              id: "do_not_need_to_change",
              name:
                currentLang === "ru-RU" ? "не надо менять" : "не треба міняти",
              price: 0,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
            },
          ],
        },
        {
          id: "ceiling",
          name: currentLang === "ru-RU" ? "Потолок" : "Стеля",
          style: {
            right: "20%",
            top: "5%",
          },
          types: [
            {
              id: "stretch_ceiling",
              name:
                currentLang === "ru-RU" ? "Натяжной потолок" : "Натяжна стеля",
              price: 450,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/ceiling/stretch_ceiling.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "painted_ceiling",
              name:
                currentLang === "ru-RU"
                  ? "Потолок под покраску"
                  : "Стеля під фарбування",
              price: 480,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/ceiling/painted_ceiling.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "plaster_ceiling",
              name: currentLang === "ru-RU" ? "Потолок гипс" : "Стеля гіпс",
              price: 750,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/ceiling/plaster_ceiling.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
        {
          id: "lighting",
          name: currentLang === "ru-RU" ? "Освещение" : "Освітлення",
          style: {
            left: "23%",
            top: "7%",
          },
          unitOfMeasurement: "шт",
          types: [
            {
              id: "chandelier",
              name: currentLang === "ru-RU" ? "Люстра" : "Люстра",
              price: 800,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/lighting/chandelier.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                  zIndex: "1",
                },
              },
            },
            {
              id: "track",
              name: currentLang === "ru-RU" ? "трековые" : "трекові",
              price: 360,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/lighting/track.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "point",
              name: currentLang === "ru-RU" ? "точненные" : "точкові",
              price: 240,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/lighting/point.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
      ],
    },
    {
      id: "room_3",
      name: currentLang === "ru-RU" ? "Комната 3" : "Кімната 3",
      area: 0,
      amount_meter: 0,
      amount: 0,
      visible: false,
      options: [
        {
          id: "floor",
          name: currentLang === "ru-RU" ? "Пол" : "Підлога",
          style: {
            left: "30%",
            bottom: "17%",
          },
          listStyle: {
            bottom: "40px",
            top: "auto",
          },
          types: [
            {
              id: "laminate",
              name: currentLang === "ru-RU" ? "Ламинат" : "Ламінат",
              price: 260,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/floor/laminate.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "quartz_vinyl",
              name: currentLang === "ru-RU" ? "кварц винил" : "кварц вініл",
              price: 400,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/floor/quartz_vinyl.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "parquet_flooring",
              name:
                currentLang === "ru-RU" ? "паркетная доска" : "паркетна дошка",
              price: 485,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/floor/parquet_flooring.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "tile",
              name:
                currentLang === "ru-RU"
                  ? "плитка (стандарт)"
                  : "плитка (стандарт)",
              price: 490,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/floor/tile.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
        {
          id: "walls",
          name: currentLang === "ru-RU" ? "Стены" : "Стіни",
          style: {
            right: "15%",
            top: "20%",
          },
          types: [
            {
              id: "wallpaper",
              name: currentLang === "ru-RU" ? "Обои" : "Шпалери",
              price: 480,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/walls/wallpaper.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "painting",
              name: currentLang === "ru-RU" ? "Покраска" : "Фарбування",
              price: 570,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/walls/painting.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "decorative_plaster",
              name:
                currentLang === "ru-RU"
                  ? "Декоративная штукатурка"
                  : "Декоративна штукатурка",
              price: 930,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/walls/decorative_plaster.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
        {
          id: "plinths",
          name: currentLang === "ru-RU" ? "Плинтуса" : "Плінтуси",
          style: {
            right: "20%",
            bottom: "20%",
          },
          listStyle: {
            bottom: "40px",
            top: "auto",
          },
          types: [
            {
              id: "plastic",
              name: currentLang === "ru-RU" ? "Пластиковый" : "Пластиковий",
              price: 60,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/plinths/plastic.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "mdf_plinth",
              name: currentLang === "ru-RU" ? "мдф плинтус" : "мдф плінтус",
              price: 135,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/plinths/mdf_plinth.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "flush_skirting_board",
              name:
                currentLang === "ru-RU"
                  ? "плинтус скрытого монтажа"
                  : "плінтус прихованого монтажу",
              price: 250,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/plinths/flush_skirting_board.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
        {
          id: "doors",
          name: currentLang === "ru-RU" ? "Двери" : "Двері",
          style: {
            right: "5%",
            bottom: "45%",
          },
          listStyle: {
            right: "0",
            left: "auto",
          },
          types: [
            {
              id: "flush_mounted_doors",
              name:
                currentLang === "ru-RU"
                  ? "Двери скрытого монтажа"
                  : "Двері прихованого монтажу",
              price: 2850,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/doors/flush_mounted_doors.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "door_with_platbands",
              name:
                currentLang === "ru-RU"
                  ? "Дверь с наличниками"
                  : "Двері з лиштвами",
              price: 2400,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/doors/door_with_platbands.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
        {
          id: "windows",
          name: currentLang === "ru-RU" ? "Окна" : "Вікна",
          style: {
            left: "10%",
            top: "40%",
          },
          types: [
            {
              id: "need_to_change",
              name: currentLang === "ru-RU" ? "надо менять" : "треба міняти",
              price: 1000,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
            },
            {
              id: "do_not_need_to_change",
              name:
                currentLang === "ru-RU" ? "не надо менять" : "не треба міняти",
              price: 0,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
            },
          ],
        },
        {
          id: "ceiling",
          name: currentLang === "ru-RU" ? "Потолок" : "Стеля",
          style: {
            right: "20%",
            top: "5%",
          },
          types: [
            {
              id: "stretch_ceiling",
              name:
                currentLang === "ru-RU" ? "Натяжной потолок" : "Натяжна стеля",
              price: 450,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/ceiling/stretch_ceiling.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "painted_ceiling",
              name:
                currentLang === "ru-RU"
                  ? "Потолок под покраску"
                  : "Стеля під фарбування",
              price: 480,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/ceiling/painted_ceiling.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "plaster_ceiling",
              name: currentLang === "ru-RU" ? "Потолок гипс" : "Стеля гіпс",
              price: 750,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/ceiling/plaster_ceiling.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
        {
          id: "lighting",
          name: currentLang === "ru-RU" ? "Освещение" : "Освітлення",
          style: {
            left: "23%",
            top: "7%",
          },
          unitOfMeasurement: "шт",
          types: [
            {
              id: "chandelier",
              name: currentLang === "ru-RU" ? "Люстра" : "Люстра",
              price: 800,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/lighting/chandelier.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                  zIndex: "1",
                },
              },
            },
            {
              id: "track",
              name: currentLang === "ru-RU" ? "трековые" : "трекові",
              price: 360,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/lighting/track.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
            {
              id: "point",
              name: currentLang === "ru-RU" ? "точненные" : "точкові",
              price: 240,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/room/lighting/point.webp",
                style: {
                  top: "0",
                  left: "0",
                  width: "100%",
                },
              },
            },
          ],
        },
      ],
    },
    {
      id: "bathroom_1",
      name: currentLang === "ru-RU" ? "Санузел 1" : "Санвузол 1",
      area: 0,
      amount_meter: 0,
      amount: 0,
      visible: false,
      options: [
        {
          id: "floor",
          name: currentLang === "ru-RU" ? "Пол" : "Підлога",
          style: {
            left: "50%",
            bottom: "20%",
          },
          types: [
            {
              id: "tile",
              name: currentLang === "ru-RU" ? "плитка" : "плитка",
              price: 507,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/floor/tile.webp",
              },
            },
            {
              id: "quartz_vinyl",
              name: currentLang === "ru-RU" ? "кварц винил" : "кварц вініл",
              price: 450,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/floor/quartz_vinyl.webp",
              },
            },
          ],
        },
        {
          id: "walls",
          name: currentLang === "ru-RU" ? "Стены" : "Стіни",
          style: {
            left: "27%",
            top: "40%",
          },
          types: [
            {
              id: "decorative_plaster",
              name:
                currentLang === "ru-RU"
                  ? "Декоративная штукатурка"
                  : "Декоративна штукатурка",
              price: 1270,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/walls/decorative_plaster.webp",
              },
            },
            {
              id: "tile",
              name: currentLang === "ru-RU" ? "плитка" : "плитка",
              price: 695,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/walls/tile.webp",
              },
            },
          ],
        },
        {
          id: "plumbing",
          name: currentLang === "ru-RU" ? "Сантехника" : "Сантехніка",
          style: {
            left: "23%",
            bottom: "25%",
            zIndex: "4",
          },
          listStyle: {
            bottom: "40px",
            top: "auto",
          },
          types: [
            {
              id: "bath",
              name:
                currentLang === "ru-RU"
                  ? "Установка ванны"
                  : "Встановлення ванни",
              price: 1550,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/plumbing/bath.webp",
              },
            },
            {
              id: "shower_cabin",
              name:
                currentLang === "ru-RU"
                  ? "Установка душевой кабины"
                  : "Встановлення душової кабіни",
              price: 2260,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/plumbing/shower_cabin.webp",
              },
            },
            {
              id: "none",
              name:
                currentLang === "ru-RU"
                  ? "Без душевой кабины или ванной"
                  : "Без душової кабіни або ванни",
              price: 0,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/plumbing/none.webp",
              },
            },
          ],
        },
        {
          id: "towel_dryer",
          name: currentLang === "ru-RU" ? "Полотенцесушитель" : "Рушникосушка",
          style: {
            right: "18%",
            bottom: "52%",
            zIndex: "6",
          },
          types: [
            {
              id: "electric_towel_dryer",
              name:
                currentLang === "ru-RU"
                  ? "електро полотенцо сушитель"
                  : "електро рушникосушарка",
              price: 550,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/towel_dryer/electric_towel_dryer.webp",
              },
            },
            {
              id: "warm_walls",
              name: currentLang === "ru-RU" ? "теплые стены" : "теплі стіни",
              price: 230,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
            },
            {
              id: "water_towel_dryer",
              name:
                currentLang === "ru-RU"
                  ? "водяной полотенцо сушитель"
                  : "водяна рушникосушарка",
              price: 850,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/towel_dryer/water_towel_dryer.webp",
              },
            },
          ],
        },
        {
          id: "toilet",
          name: currentLang === "ru-RU" ? "Унитаз" : "Унітаз",
          style: {
            right: "22%",
            bottom: "30%",
            zIndex: "5",
          },
          types: [
            {
              id: "floor_toilet",
              name:
                currentLang === "ru-RU"
                  ? "Установка напольного унитаза"
                  : "Встановлення підлогового унітазу",
              price: 1000,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/toilet/floor_toilet.webp",
              },
            },
            {
              id: "wall_hung_toilet",
              name:
                currentLang === "ru-RU"
                  ? "Установка подвесного унитаза"
                  : "Встановлення підвісного унітазу",
              price: 850,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/toilet/wall_hung_toilet.webp",
              },
            },
          ],
        },
        {
          id: "bidet",
          name: currentLang === "ru-RU" ? "Биде" : "Біде",
          style: {
            right: "19%",
            bottom: "22%",
          },
          types: [
            {
              id: "bidet",
              name: currentLang === "ru-RU" ? "надо" : "треба",
              price: 800,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/bidet/bidet.webp",
              },
            },
            {
              id: "none",
              name: currentLang === "ru-RU" ? "не надо" : "не треба",
              price: 0,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
            },
            {
              id: "hygienic_shower",
              name:
                currentLang === "ru-RU"
                  ? "гигинический душ"
                  : "гігієнічний душ",
              price: 1100,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/bidet/hygienic_shower.webp",
              },
            },
          ],
        },
        {
          id: "ceiling",
          name: currentLang === "ru-RU" ? "Потолок" : "Стеля",
          style: {
            right: "25%",
            top: "7%",
          },
          types: [
            {
              id: "tension",
              name: currentLang === "ru-RU" ? "Натяжной" : "Натяжна",
              price: 450,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/ceiling/tension.webp",
              },
            },
            {
              id: "plasterboard",
              name: currentLang === "ru-RU" ? "С ГКЛ" : "З ГКЛ",
              price: 750,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/ceiling/plasterboard.webp",
              },
            },
            {
              id: "plastering_and_painting",
              name:
                currentLang === "ru-RU"
                  ? "поштукатурка и покраска"
                  : "поштукатурка і фарбування",
              price: 480,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
            },
          ],
        },
        {
          id: "lighting",
          name: currentLang === "ru-RU" ? "Освещение" : "Освітлення",
          style: {
            left: "26%",
            top: "7%",
          },
          unitOfMeasurement: "шт",
          types: [
            {
              id: "track",
              name: currentLang === "ru-RU" ? "трековые" : "трекові",
              price: 360,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/lighting/track.webp",
              },
            },
            {
              id: "point",
              name: currentLang === "ru-RU" ? "точненные" : "точкові",
              price: 240,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/lighting/point.webp",
              },
            },
          ],
        },
      ],
    },
    {
      id: "bathroom_2",
      name: currentLang === "ru-RU" ? "Санузел 2" : "Санвузол 2",
      area: 0,
      amount_meter: 0,
      amount: 0,
      visible: false,
      options: [
        {
          id: "floor",
          name: currentLang === "ru-RU" ? "Пол" : "Підлога",
          style: {
            left: "50%",
            bottom: "20%",
          },
          types: [
            {
              id: "tile",
              name: currentLang === "ru-RU" ? "плитка" : "плитка",
              price: 507,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/floor/tile.webp",
              },
            },
            {
              id: "quartz_vinyl",
              name: currentLang === "ru-RU" ? "кварц винил" : "кварц вініл",
              price: 450,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/floor/quartz_vinyl.webp",
              },
            },
          ],
        },
        {
          id: "walls",
          name: currentLang === "ru-RU" ? "Стены" : "Стіни",
          style: {
            left: "27%",
            top: "40%",
          },
          types: [
            {
              id: "decorative_plaster",
              name:
                currentLang === "ru-RU"
                  ? "Декоративная штукатурка"
                  : "Декоративна штукатурка",
              price: 1270,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/walls/decorative_plaster.webp",
              },
            },
            {
              id: "tile",
              name: currentLang === "ru-RU" ? "плитка" : "плитка",
              price: 695,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/walls/tile.webp",
              },
            },
          ],
        },
        {
          id: "plumbing",
          name: currentLang === "ru-RU" ? "Сантехника" : "Сантехніка",
          style: {
            left: "23%",
            bottom: "25%",
            zIndex: "4",
          },
          listStyle: {
            bottom: "40px",
            top: "auto",
          },
          types: [
            {
              id: "bath",
              name:
                currentLang === "ru-RU"
                  ? "Установка ванны"
                  : "Встановлення ванни",
              price: 1550,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/plumbing/bath.webp",
              },
            },
            {
              id: "shower_cabin",
              name:
                currentLang === "ru-RU"
                  ? "Установка душевой кабины"
                  : "Встановлення душової кабіни",
              price: 2260,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/plumbing/shower_cabin.webp",
              },
            },
            {
              id: "none",
              name:
                currentLang === "ru-RU"
                  ? "Без душевой кабины или ванной"
                  : "Без душової кабіни або ванни",
              price: 0,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/plumbing/none.webp",
              },
            },
          ],
        },
        {
          id: "towel_dryer",
          name: currentLang === "ru-RU" ? "Полотенцесушитель" : "Рушникосушка",
          style: {
            right: "18%",
            bottom: "52%",
            zIndex: "6",
          },
          types: [
            {
              id: "electric_towel_dryer",
              name:
                currentLang === "ru-RU"
                  ? "електро полотенцо сушитель"
                  : "електро рушникосушарка",
              price: 550,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/towel_dryer/electric_towel_dryer.webp",
              },
            },
            {
              id: "warm_walls",
              name: currentLang === "ru-RU" ? "теплые стены" : "теплі стіни",
              price: 230,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
            },
            {
              id: "water_towel_dryer",
              name:
                currentLang === "ru-RU"
                  ? "водяной полотенцо сушитель"
                  : "водяна рушникосушарка",
              price: 850,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/towel_dryer/water_towel_dryer.webp",
              },
            },
          ],
        },
        {
          id: "toilet",
          name: currentLang === "ru-RU" ? "Унитаз" : "Унітаз",
          style: {
            right: "22%",
            bottom: "30%",
            zIndex: "5",
          },
          types: [
            {
              id: "floor_toilet",
              name:
                currentLang === "ru-RU"
                  ? "Установка напольного унитаза"
                  : "Встановлення підлогового унітазу",
              price: 1000,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/toilet/floor_toilet.webp",
              },
            },
            {
              id: "wall_hung_toilet",
              name:
                currentLang === "ru-RU"
                  ? "Установка подвесного унитаза"
                  : "Встановлення підвісного унітазу",
              price: 850,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/toilet/wall_hung_toilet.webp",
              },
            },
          ],
        },
        {
          id: "bidet",
          name: currentLang === "ru-RU" ? "Биде" : "Біде",
          style: {
            right: "19%",
            bottom: "22%",
          },
          types: [
            {
              id: "bidet",
              name: currentLang === "ru-RU" ? "надо" : "треба",
              price: 800,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/bidet/bidet.webp",
              },
            },
            {
              id: "none",
              name: currentLang === "ru-RU" ? "не надо" : "не треба",
              price: 0,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
            },
            {
              id: "hygienic_shower",
              name:
                currentLang === "ru-RU"
                  ? "гигинический душ"
                  : "гігієнічний душ",
              price: 1100,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/bidet/hygienic_shower.webp",
              },
            },
          ],
        },
        {
          id: "ceiling",
          name: currentLang === "ru-RU" ? "Потолок" : "Стеля",
          style: {
            right: "25%",
            top: "7%",
          },
          types: [
            {
              id: "tension",
              name: currentLang === "ru-RU" ? "Натяжной" : "Натяжна",
              price: 450,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/ceiling/tension.webp",
              },
            },
            {
              id: "plasterboard",
              name: currentLang === "ru-RU" ? "С ГКЛ" : "З ГКЛ",
              price: 750,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/ceiling/plasterboard.webp",
              },
            },
            {
              id: "plastering_and_painting",
              name:
                currentLang === "ru-RU"
                  ? "поштукатурка и покраска"
                  : "поштукатурка і фарбування",
              price: 480,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
            },
          ],
        },
        {
          id: "lighting",
          name: currentLang === "ru-RU" ? "Освещение" : "Освітлення",
          style: {
            left: "26%",
            top: "7%",
          },
          unitOfMeasurement: "шт",
          types: [
            {
              id: "track",
              name: currentLang === "ru-RU" ? "трековые" : "трекові",
              price: 360,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/lighting/track.webp",
              },
            },
            {
              id: "point",
              name: currentLang === "ru-RU" ? "точненные" : "точкові",
              price: 240,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/bathroom/lighting/point.webp",
              },
            },
          ],
        },
      ],
    },
    {
      id: "studio_1",
      name: currentLang === "ru-RU" ? "Студия" : "Студія",
      area: 0,
      amount_meter: 0,
      amount: 0,
      visible: false,
      options: [
        {
          id: "walls",
          name: currentLang === "ru-RU" ? "Стены" : "Стіни",
          style: {
            right: "10%",
            top: "30%",
          },
          types: [
            {
              id: "wallpaper",
              name: currentLang === "ru-RU" ? "Обои" : "Шпалери",
              price: 480,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/walls/wallpaper.webp",
              },
            },
            {
              id: "painting",
              name: currentLang === "ru-RU" ? "Покраска" : "Фарбування",
              price: 570,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/walls/painting.webp",
              },
            },
            {
              id: "decorative_plaster",
              name:
                currentLang === "ru-RU"
                  ? "Декоративная штукатурка"
                  : "Декоративна штукатурка",
              price: 930,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/walls/decorative_plaster.webp",
              },
            },
          ],
        },
        {
          id: "doors",
          name: currentLang === "ru-RU" ? "Двери" : "Двері",
          style: {
            right: "5%",
            bottom: "35%",
            zIndex: "6",
          },
          listStyle: {
            right: 0,
            left: "auto",
          },
          types: [
            {
              id: "flush_mounted_doors",
              name:
                currentLang === "ru-RU"
                  ? "Двери скрытого монтажа"
                  : "Двері прихованого монтажу",
              price: 2850,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/doors/flush_mounted_doors.webp",
              },
            },
            {
              id: "door_with_platbands",
              name:
                currentLang === "ru-RU"
                  ? "Дверь с наличниками"
                  : "Двері з лиштвами",
              price: 2400,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/doors/door_with_platbands.webp",
              },
            },
          ],
        },
        {
          id: "skirting",
          name: currentLang === "ru-RU" ? "Плинтус" : "Плінтус",
          style: {
            right: "18%",
            bottom: "16%",
            zIndex: "5",
          },
          listStyle: {
            bottom: "40px",
            top: "auto",
            right: 0,
            left: "auto",
          },
          types: [
            {
              id: "plastic",
              name: currentLang === "ru-RU" ? "Пластиковый" : "Пластиковий",
              price: 80,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/skirting/plastic.webp",
              },
            },
            {
              id: "mdf_skirting_board",
              name: currentLang === "ru-RU" ? "мдф плинтус" : "мдф плінтус",
              price: 140,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/skirting/mdf_skirting_board.webp",
              },
            },
            {
              id: "flush_skirting_board",
              name:
                currentLang === "ru-RU"
                  ? "плинтус скрытого монтажа"
                  : "плінтус прихованого монтажу",
              price: 260,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/skirting/flush_skirting_board.webp",
              },
            },
          ],
        },
        {
          id: "ceiling",
          name: currentLang === "ru-RU" ? "Потолок" : "Стеля",
          style: {
            right: "15%",
            top: "5%",
          },
          types: [
            {
              id: "stretch_ceiling",
              name:
                currentLang === "ru-RU" ? "Натяжной потолок" : "Натяжна стеля",
              price: 450,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/ceiling/stretch_ceiling.webp",
              },
            },
            {
              id: "painted_ceiling",
              name:
                currentLang === "ru-RU"
                  ? "Потолок под покраску"
                  : "Стеля під фарбування",
              price: 480,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/ceiling/painted_ceiling.webp",
              },
            },
            {
              id: "plaster_ceiling",
              name: currentLang === "ru-RU" ? "Потолок гипс" : "Стеля гіпс",
              price: 750,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/ceiling/plaster_ceiling.webp",
              },
            },
          ],
        },
        {
          id: "lighting",
          name: currentLang === "ru-RU" ? "Освещение" : "Освітлення",
          style: {
            left: "20%",
            top: "5%",
          },
          types: [
            {
              id: "chandelier",
              name: currentLang === "ru-RU" ? "Люстра" : "Люстра",
              unitOfMeasurement: "шт",
              isMeter: false,
              price: 800,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/lighting/chandelier.webp",
              },
            },
            {
              id: "track",
              name: currentLang === "ru-RU" ? "трековые" : "трекові",
              price: 360,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/lighting/track.webp",
              },
            },
            {
              id: "point_illumination",
              name:
                currentLang === "ru-RU"
                  ? "освещение точки"
                  : "освітлення точки",
              price: 200,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/lighting/point_illumination.webp",
              },
            },
          ],
        },
        {
          id: "windows",
          name: currentLang === "ru-RU" ? "Окна" : "Вікна",
          style: {
            left: "5%",
            top: "40%",
          },
          types: [
            {
              id: "need_to_change",
              name: currentLang === "ru-RU" ? "надо менять" : "треба міняти",
              price: 1000,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: true,
            },
            {
              id: "do_not_change",
              name:
                currentLang === "ru-RU" ? "не надо менять" : "не треба міняти",
              price: 0,
              unitOfMeasurement: "шт",
              isMeter: false,
              isSelected: false,
            },
          ],
        },
        {
          id: "floor",
          name: currentLang === "ru-RU" ? "Пол" : "Підлога",
          style: {
            left: "23%",
            bottom: "15%",
          },
          listStyle: {
            bottom: "40px",
            top: "auto",
          },
          types: [
            {
              id: "laminate",
              name: currentLang === "ru-RU" ? "Ламинат" : "Ламінат",
              price: 260,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/floor/laminate.webp",
              },
            },
            {
              id: "quartz_vinyl",
              name: currentLang === "ru-RU" ? "кварц винил" : "кварц вініл",
              price: 400,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/floor/quartz_vinyl.webp",
              },
            },
            {
              id: "parquet_flooring",
              name:
                currentLang === "ru-RU" ? "паркетная доска" : "паркетна дошка",
              price: 485,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/floor/parquet_flooring.webp",
              },
            },
            {
              id: "tile",
              name:
                currentLang === "ru-RU"
                  ? "плитка (стандарт)"
                  : "плитка (стандарт)",
              price: 490,
              unitOfMeasurement: "м2",
              isMeter: true,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/floor/tile.webp",
              },
            },
          ],
        },
        {
          id: "apron",
          name: currentLang === "ru-RU" ? "Фартук" : "Фартух",
          style: {
            left: "50%",
            top: "50%",
          },
          listStyle: {
            bottom: "40px",
            top: "auto",
          },
          types: [
            {
              id: "ceramic_tile_apron",
              name:
                currentLang === "ru-RU"
                  ? "Фартук из керамической плитки"
                  : "Фартух із керамічної плитки",
              price: 550,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: true,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/apron/ceramic_tile_apron.webp",
              },
            },
            {
              id: "apron_made_of_mdf_panels",
              name:
                currentLang === "ru-RU"
                  ? "Фартук из МДФ панелей"
                  : "Фартух із МДФ панелей",
              price: 2200,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/apron/apron_made_of_mdf_panels.webp",
              },
            },
            {
              id: "artificial_stone_apron",
              name:
                currentLang === "ru-RU"
                  ? "Фартук из искусственного камня"
                  : "Фартух зі штучного каменю",
              price: 3500,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/apron/artificial_stone_apron.webp",
              },
            },
            {
              id: "skenali_glass_aprons",
              name:
                currentLang === "ru-RU"
                  ? "Стеклянные фартуки скенали"
                  : "Скляні фартухи скеналі",
              price: 2000,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/apron/skenali_glass_aprons.webp",
              },
            },
            {
              id: "wall_panels",
              name:
                currentLang === "ru-RU" ? "стеновые панели" : "стінові панелі",
              price: 2000,
              unitOfMeasurement: "м.п.",
              isMeter: false,
              isSelected: false,
              image: {
                src: "https://www.vidbydova.com.ua/assets/img/kitchen/apron/wall_panels.webp",
              },
            },
          ],
        },
      ],
    },
  ]);
  const changeVisibleRoomHandler = (id, visible) => {
    const updatedRooms = [...rooms];
    const changeRoomIndex = updatedRooms.findIndex((room) => room.id === id);

    if (changeRoomIndex !== -1) {
      updatedRooms[changeRoomIndex].visible = visible;
      if (!visible) {
        updatedRooms[changeRoomIndex].area = 0;
        updatedRooms[changeRoomIndex].amount = 0;
        updatedRooms[changeRoomIndex].amount_meter = 0;
        updateApartmentsAreaHandler();
      }
      setRooms(updatedRooms);
    }
  };

  const changeRoomAreaHandler = (id, area) => {
    const updatedRooms = [...rooms];
    const changeRoomIndex = updatedRooms.findIndex((room) => room.id === id);

    if (changeRoomIndex !== -1) {
      updatedRooms[changeRoomIndex].area = area;
      setRooms(updatedRooms);
      updateApartmentsAreaHandler();
    }
  };

  const changeRoomOptionsHandler = (roomId, optionItemId, typeItemId) => {
    const updatedRooms = [...rooms];
    const changeRoomIndex = updatedRooms.findIndex(
      (room) => room.id === roomId
    );

    if (changeRoomIndex !== -1) {
      const changeOptionIndex = updatedRooms[changeRoomIndex].options.findIndex(
        (option) => option.id === optionItemId
      );
      if (changeOptionIndex !== -1) {
        const changeTypeIndex = updatedRooms[changeRoomIndex].options[
          changeOptionIndex
        ].types.findIndex((type) => type.id === typeItemId);
        if (changeTypeIndex !== -1) {
          updatedRooms[changeRoomIndex].options[
            changeOptionIndex
          ].types.forEach((item) => (item.isSelected = false));
          updatedRooms[changeRoomIndex].options[changeOptionIndex].types[
            changeTypeIndex
          ].isSelected = true;
        }
      }
    }

    setRooms(updatedRooms);
    updateRoomCostHandler(roomId);
  };

  const updateRoomCostHandler = (roomId) => {
    const updatedRooms = [...rooms];
    const changeRoomIndex = updatedRooms.findIndex(
      (room) => room.id === roomId
    );

    if (changeRoomIndex !== -1) {
      const cost_meter = updatedRooms[changeRoomIndex].options.map(
        (option) =>
          option.types.filter((type) => type.isSelected && type.isMeter)[0]
            ?.price ?? 0
      );
      updatedRooms[changeRoomIndex].amount_meter = cost_meter.reduce(
        (accumulator, current) => accumulator + current,
        0
      );

      const cost_non_meter = updatedRooms[changeRoomIndex].options.map(
        (option) =>
          option.types.filter((type) => type.isSelected && !type.isMeter)[0]
            ?.price ?? 0
      );
      updatedRooms[changeRoomIndex].amount =
        cost_non_meter.reduce(
          (accumulator, current) => accumulator + current,
          0
        ) +
        updatedRooms[changeRoomIndex].amount_meter *
          updatedRooms[changeRoomIndex].area;

      // updatedRooms[changeRoomIndex].amount =
      //   updatedRooms[changeRoomIndex].amount_meter *
      //   updatedRooms[changeRoomIndex].area;
    }

    setRooms(updatedRooms);
    changeTotalCostHandler();
  };

  const [additionalOptions, setAdditionalOptions] = useState([
    {
      id: "plaster",
      optionName: currentLang === "ru-RU" ? "Штукатурка" : "Штукатурка",
      price: 180,
      isSelected: false,
      isMeter: true,
    },
    {
      id: "ceiling_soundproofing",
      optionName:
        currentLang === "ru-RU"
          ? "Звукоизоляция потолка"
          : "Звукоізоляція стелі",
      price: 250,
      isSelected: false,
      isMeter: true,
    },
    {
      id: "floor_soundproofing",
      optionName:
        currentLang === "ru-RU"
          ? "Звукоизоляция пола"
          : "Звукоізоляція підлоги",
      price: 330,
      isSelected: false,
      isMeter: true,
    },
    {
      id: "wall_soundproofing",
      optionName:
        currentLang === "ru-RU" ? "Звукоизоляция стен" : "Звукоізоляція стін",
      price: 290,
      isSelected: false,
      isMeter: true,
    },
    {
      id: "front_door_replacement",
      optionName:
        currentLang === "ru-RU"
          ? "Замена входной двери"
          : "Заміна вхідних дверей",
      price: 150,
      isSelected: false,
      isMeter: true,
    },
    {
      id: "floor_heating",
      optionName:
        currentLang === "ru-RU"
          ? "Теплый пол санузел, коридор 12м2"
          : "Тепла підлога санвузол, коридор 12м2",
      price: 265,
      isSelected: false,
      isMeter: true,
    },
    {
      id: "mdf_panels",
      optionName:
        currentLang === "ru-RU"
          ? "МДФ панели (расчет на 20м2 стен комнат)"
          : "МДФ панелі (розрахунок на 20м2 стін кімнат)",
      price: 2000,
      isSelected: false,
      isMeter: true,
    },
    {
      id: "wooden_laths",
      optionName:
        currentLang === "ru-RU"
          ? "Деревянные рейки (расчет на 20м2 стен комнат)"
          : "Дерев'яні рейки (розрахунок на 20м2 стін кімнат)",
      price: 3000,
      isSelected: false,
      isMeter: true,
    },
    {
      id: "smart_home_system",
      optionName:
        currentLang === "ru-RU"
          ? "Система умный дом"
          : "Система розумний будинок",
      price: 2500,
      isSelected: false,
      isMeter: false,
    },
  ]);
  const changeOptionSelectHandler = (id) => {
    const updatedAdditionalOptions = [...additionalOptions];
    const changeOptionIndex = updatedAdditionalOptions.findIndex(
      (option) => option.id === id
    );

    if (changeOptionIndex !== -1) {
      updatedAdditionalOptions[changeOptionIndex].isSelected =
        !updatedAdditionalOptions[changeOptionIndex].isSelected;
      setAdditionalOptions(updatedAdditionalOptions);
      changeTotalCostHandler();
    }
  };

  const [steps, setSteps] = useState([
    {
      id: 1,
      step: currentLang === "ru-RU" ? "Параметры" : "Параметри",
      visible: true,
      active: true,
    },
    {
      id: 2,
      step: currentLang === "ru-RU" ? "Комнаты" : "Кімнати",
      visible: true,
      active: false,
    },
    {
      id: 3,
      step: currentLang === "ru-RU" ? "Состояние" : "Стан",
      visible: true,
      active: false,
    },
    {
      id: 4,
      slug: "kitchen_1",
      step: currentLang === "ru-RU" ? "Кухня" : "Кухня",
      visible: false,
      active: false,
    },
    {
      id: 5,
      slug: "room_1",
      step: currentLang === "ru-RU" ? "Комната 1" : "Кімната 1",
      visible: false,
      active: false,
    },
    {
      id: 6,
      slug: "room_2",
      step: currentLang === "ru-RU" ? "Комната 2" : "Кімната 2",
      visible: false,
      active: false,
    },
    {
      id: 7,
      slug: "room_3",
      step: currentLang === "ru-RU" ? "Комната 3" : "Кімната 3",
      visible: false,
      active: false,
    },
    {
      id: 8,
      slug: "bathroom_1",
      step: currentLang === "ru-RU" ? "Санузел 1" : "Санвузол 1",
      visible: false,
      active: false,
    },
    {
      id: 9,
      slug: "bathroom_2",
      step: currentLang === "ru-RU" ? "Санузел 2" : "Санвузол 2",
      visible: false,
      active: false,
    },
    {
      id: 10,
      slug: "studio_1",
      step: currentLang === "ru-RU" ? "Студия" : "Студія",
      visible: false,
      active: false,
    },
    {
      id: 11,
      step: currentLang === "ru-RU" ? "Доп. опции" : "Дод. опції",
      visible: true,
      active: false,
    },
    {
      id: 12,
      step: currentLang === "ru-RU" ? "Рассчет" : "Розрахунок",
      visible: true,
      active: false,
    },
  ]);
  const changeStepVisibleHandler = (slug, visible) => {
    const updatedSteps = [...steps];
    const changeStepIndex = updatedSteps.findIndex(
      (step) => step.slug === slug
    );

    if (changeStepIndex !== -1) {
      updatedSteps[changeStepIndex].visible = visible;
      setSteps(updatedSteps);
    }
  };

  const changeActiveStepHandler = (nextStep) => {
    const newSteps = [...steps];
    newSteps.forEach((step) => {
      if (step.id === nextStep) {
        step.active = true;
      } else {
        step.active = false;
      }
    });
    setSteps(newSteps);
  };

  const [apartmentsArea, setApartmentsArea] = useState(0);
  const updateApartmentsAreaHandler = () => {
    let sum = 0;
    rooms.forEach((room) => {
      sum += room.area;
    });
    setApartmentsArea(sum);
  };

  const [totalCost, setTotalCost] = useState({
    cost: 0,
    cost_for_square_meter: 0,
  });
  const changeTotalCostHandler = () => {
    let costMeter = 0;
    let cost = 0;
    const selectedOptions = additionalOptions.filter(
      (option) => option.isSelected
    );
    selectedOptions.forEach((option) => {
      if (option.isMeter) {
        costMeter += option.price;
        cost += option.price * apartmentsArea;
      } else {
        cost += option.price;
      }
    });
    const newCost = {
      cost:
        rooms
          .map((room) => room.amount)
          .reduce((accumulator, current) => accumulator + current, 0) + cost,
      cost_for_square_meter:
        rooms
          .map((room) => room.amount_meter)
          .reduce((accumulator, current) => accumulator + current, 0) +
        costMeter,
    };

    setTotalCost(newCost);
  };

  const [info, setInfo] = useState({
    city: "",
    objectStatus: currentLang === "ru-RU" ? "Новостройка" : "Новобудова",
    street: "",
    ceilingHeight: currentLang === "ru-RU" ? "До 3 м" : "До 3 м",
    conditionWalls: currentLang === "ru-RU" ? "Голые стены" : "Голі стіни",
    conditionFloors:
      currentLang === "ru-RU" ? "Полы без стяжки" : "Підлога без стяжки",
    phone: "",
    messenger: "viber",
  });
  const updateInfoHandler = (cell, value) => {
    setInfo({ ...info, [cell]: value });
  };

  const sendResult = (e) => {
    e.preventDefault();

    const selectedRooms = rooms.filter((room) => room.visible);
    let roomsData = [];
    let optionsRoom = [];
    let additional = [];

    selectedRooms.forEach((room) => {
      room.options.forEach((option) => {
        const optionType = option.types.filter((item) => item.isSelected)[0];
        optionsRoom = [
          ...optionsRoom,
          {
            name: option.name,
            selectItem: {
              name: optionType.name,
              price: optionType.price,
            },
          },
        ];
      });

      roomsData = [
        ...roomsData,
        {
          name: room.name,
          area: room.area,
          amount: room.amount,
          amount_meter: room.amount_meter,
          options: optionsRoom,
        },
      ];
    });

    additionalOptions
      .filter((option) => option.isSelected)
      .forEach((option) => {
        additional = [
          ...additional,
          {
            name: option.optionName,
            price: option.price,
          },
        ];
      });

    const data = {
      city: info.city,
      objectStatus: info.objectStatus,
      street: info.street,
      ceilingHeight: info.ceilingHeight,
      conditionWalls: info.conditionWalls,
      conditionFloors: info.conditionFloors,
      rooms: roomsData,
      additional: additional,
      totalCost: totalCost.cost,
      totalCostMeter: totalCost.cost_for_square_meter,
      apartmentsArea: apartmentsArea,
      months: apartmentsArea <= 80 ? 4 : 5 + " мес.",
      phone: info.phone,
      messenger: info.messenger,
    };

    fetch("https://www.vidbydova.com.ua/wp-json/wp/v2/calculator-result", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      // .then((response) => {
      // console.log(response);
      // const tokenBot = "7385518774:AAE4BKwsoDnjFHuq_a2DmSOKit3P-Ej8Ims";
      // const chatId = "562491180";
      // fetch(`https://api.telegram.org/bot${tokenBot}/sendMessage`, {
      //   method: "POST",
      //   headers: {
      //     "Content-Type": "application/json",
      //   },
      //   body: JSON.stringify({
      //     chat_id: chatId,
      //     // text: "test",
      //     text: response,
      //   }),
      // });
      // })
      .then(() =>
        Swal.fire({
          icon: "success",
          title:
            currentLang === "ru-RU" ? "Спасибо за заявку" : "Дякую за заявку",
          text:
            currentLang === "ru-RU"
              ? "Мы свяжемся с Вами в ближайшее время"
              : "Ми зв'яжемося з Вами найближчим часом",
        })
      )
      .catch(() =>
        Swal.fire({
          icon: "error",
          title: currentLang === "ru-RU" ? "Ошибка" : "Помилка",
          text:
            currentLang === "ru-RU"
              ? "Что-то пошло не так"
              : "Щось пішло не так",
        })
      );
  };

  return (
    <form method="post" className="calculator-new" onSubmit={sendResult}>
      {steps[0].active && (
        <ParametersStep
          info={info}
          updateInfo={updateInfoHandler}
          steps={steps}
          changeActiveStep={changeActiveStepHandler}
          currentLang={currentLang}
        />
      )}
      {steps[1].active && (
        <RoomsStep
          apartmentsArea={apartmentsArea}
          rooms={rooms}
          steps={steps}
          changeStepVisible={changeStepVisibleHandler}
          changeVisibleRoom={changeVisibleRoomHandler}
          changeRoomArea={changeRoomAreaHandler}
          changeActiveStep={changeActiveStepHandler}
          currentLang={currentLang}
        />
      )}
      {steps[2].active && (
        <ConditionStep
          updateInfo={updateInfoHandler}
          steps={steps}
          changeActiveStep={changeActiveStepHandler}
          currentLang={currentLang}
        />
      )}
      {steps[3].active && (
        <KitchenStep
          steps={steps}
          room={rooms[0]}
          cost={totalCost.cost_for_square_meter}
          changeActiveStep={changeActiveStepHandler}
          changeRoomOptions={changeRoomOptionsHandler}
          updateRoomCost={updateRoomCostHandler}
          currentLang={currentLang}
        />
      )}
      {steps[4].active && (
        <RoomStep
          steps={steps}
          room={rooms[1]}
          cost={totalCost.cost_for_square_meter}
          changeActiveStep={changeActiveStepHandler}
          changeRoomOptions={changeRoomOptionsHandler}
          updateRoomCost={updateRoomCostHandler}
          currentLang={currentLang}
        />
      )}
      {steps[5].active && (
        <Room2Step
          steps={steps}
          room={rooms[2]}
          cost={totalCost.cost_for_square_meter}
          changeActiveStep={changeActiveStepHandler}
          changeRoomOptions={changeRoomOptionsHandler}
          updateRoomCost={updateRoomCostHandler}
          currentLang={currentLang}
        />
      )}
      {steps[6].active && (
        <Room3Step
          steps={steps}
          room={rooms[3]}
          cost={totalCost.cost_for_square_meter}
          changeActiveStep={changeActiveStepHandler}
          changeRoomOptions={changeRoomOptionsHandler}
          updateRoomCost={updateRoomCostHandler}
          currentLang={currentLang}
        />
      )}
      {steps[7].active && (
        <BathroomStep
          steps={steps}
          room={rooms[4]}
          cost={totalCost.cost_for_square_meter}
          changeActiveStep={changeActiveStepHandler}
          changeRoomOptions={changeRoomOptionsHandler}
          updateRoomCost={updateRoomCostHandler}
          currentLang={currentLang}
        />
      )}
      {steps[8].active && (
        <Bathroom2Step
          steps={steps}
          room={rooms[5]}
          cost={totalCost.cost_for_square_meter}
          changeActiveStep={changeActiveStepHandler}
          changeRoomOptions={changeRoomOptionsHandler}
          updateRoomCost={updateRoomCostHandler}
          currentLang={currentLang}
        />
      )}
      {steps[9].active && (
        <StudioStep
          steps={steps}
          room={rooms[6]}
          cost={totalCost.cost_for_square_meter}
          changeActiveStep={changeActiveStepHandler}
          changeRoomOptions={changeRoomOptionsHandler}
          updateRoomCost={updateRoomCostHandler}
          currentLang={currentLang}
        />
      )}
      {steps[10].active && (
        <AdditionalOptionsStep
          additionalOptions={additionalOptions}
          steps={steps}
          changeActiveStep={changeActiveStepHandler}
          changeOptionSelect={changeOptionSelectHandler}
          currentLang={currentLang}
        />
      )}
      {steps[11].active && (
        <ResultStep
          phone={info.phone}
          updateInfo={updateInfoHandler}
          totalCost={totalCost}
          apartmentsArea={apartmentsArea}
          steps={steps}
          changeActiveStep={changeActiveStepHandler}
          messenger={info.messenger}
          currentLang={currentLang}
        />
      )}
    </form>
  );
}

export default App;
